%undefine __cmake_in_source_build

%bcond_without sys_llvm
%bcond_without check
%bcond_with classic_flang
%bcond_with toolchain_clang
%bcond_without bisheng_autotuner
%bcond_without ACPO

%if %{with toolchain_clang}
%global toolchain clang
%endif

%global maj_ver 17
%global min_ver 0
%global patch_ver 6

%if %{with sys_llvm}
%global pkg_name llvm
%global install_prefix %{_prefix}
%global install_datadir %{_datadir}
%else
%global pkg_name llvm%{maj_ver}
%global install_prefix %{_libdir}/%{name}
%global install_datadir %{install_prefix}/share
%endif

%global install_bindir %{install_prefix}/bin
%global install_includedir %{install_prefix}/include
%if 0%{?__isa_bits} == 64
%global install_libdir %{install_prefix}/lib64
%else
%global install_libdir %{install_prefix}/lib
%endif
%global install_srcdir %{install_prefix}/src
%global install_docdir %{install_prefix}/share/doc

%global max_link_jobs %{_smp_build_ncpus}
%global targets_to_build "all"
%global experimental_targets_to_build ""

%global build_install_prefix %{buildroot}%{install_prefix}
%global llvm_triple %{_host}

# Disable LTO as this causes crash if gcc lto enabled.
%define _lto_cflags %{nil}

Name:		%{pkg_name}
Version:	%{maj_ver}.%{min_ver}.%{patch_ver}
Release:	29
Summary:	The Low Level Virtual Machine

License:	NCSA
URL:		http://llvm.org
Source0:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{version}/llvm-%{version}.src.tar.xz
Source1:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{version}/cmake-%{version}.src.tar.xz
Source2:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{version}/third-party-%{version}.src.tar.xz

# Patch{1-10} for supporting `relax` feture on LoongArch, which is consistent with !47 in openEuler repos
Patch1: 	0001-Backport-LoongArch-Add-relax-feature-and-keep-relocations.patch
Patch2: 	0002-Backport-LoongArch-Allow-delayed-decision-for-ADD-SUB-relocations.patch
Patch3: 	0003-Backport-LoongArch-Emit-R_LARCH_RELAX-when-expanding-some-LoadAddress.patch
Patch4: 	0004-Backport-MC-LoongArch-Add-AlignFragment-size-if-layout-is-available-and-not-need-insert-nops.patch
Patch5: 	0005-Backport-LoongArch-RISCV-Support-R_LARCH_-ADD-SUB-_ULEB128-R_RISCV_-SET-SUB-_ULEB128-for-uleb128-directives.patch
Patch6: 	0006-Backport-LoongArch-Add-relaxDwarfLineAddr-and-relaxDwarfCFA-to-handle-the-mutable-label-diff-in-dwarfinfo.patch
Patch7: 	0007-Backport-LoongArch-Insert-nops-and-emit-align-reloc-when-handle-alignment-directive.patch
Patch8: 	0008-Backport-test-Update-dwarf-loongarch-relocs.ll.patch
Patch9: 	0009-Backport-MC-test-Change-ELF-uleb-ehtable.s-Mach-O-to-use-private-symbols-in-.uleb128-for-label-differences.patch
Patch10: 	0010-Backport-Mips-MC-AttemptToFoldSymbolOffsetDifference-revert-isMicroMips-special-case.patch

Patch11: 	0011-Backport-LoongArch-Add-the-support-for-vector-in-llvm17.patch
Patch12: 	0012-Backport-LoongArch-improve-the-support-for-compiler-rt-and-bugfix.patch
Patch13: 	0013-Backport-Bitcode-Add-some-missing-GetTypeByID-failure-checks.patch
Patch14: 	0014-Backport-X86-Inline-Skip-inline-asm-in-inlining-targ.patch
Patch15: 	0015-Backport-ARM-Check-all-terms-in-emitPopInst-when-clearing-Res.patch
Patch16: 	0016-Backport-ARM-Update-IsRestored-for-LR-based-on-all-returns-82.patch
Patch17:	0017-Add-the-support-for-classic-flang.patch
Patch18:	0018-Fix-declaration-definition-mismatch-for-classic-flang.patch
Patch19: 	0019-Backport-LoongArch-Improve-the-support-for-atomic-and-clear_cache.patch
Patch20: 	0020-Update-llvm-lit-config-to-support-build_for_openeule.patch
Patch21:	0021-Add-BiSheng-Autotuner-support-for-LLVM-compiler.patch
Patch22:	0022-Prevent-environment-variables-from-exceeding-NAME_MA.patch
Patch23:	0023-AArch64-Support-HiSilicon-s-HIP09-Processor.patch
Patch24:	0024-Backport-LoongArch-fix-and-add-some-new-support.patch
Patch25:    0025-Backport-Simple-check-to-ignore-Inline-asm-fwait-insertion.patch
Patch26:    0026-Add-arch-restriction-for-BiSheng-Autotuner.patch
Patch27:	0027-AArch64-Delete-hip09-macro.patch
Patch28:	0028-backport-Clang-Fix-crash-with-fzero-call-used-regs.patch
Patch29:	0029-SimplifyLibCalls-Merge-sqrt-into-the-power-of-exp-79.patch
Patch30:	0030-LICM-Solve-runtime-error-caused-by-the-signal-functi.patch
Patch31:	0031-ACPO-ACPO-Infrastructure.patch
Patch32:	0032-ACPO-Introduce-MLInliner-using-ACPO-infrastructure.patch
Patch33:	0033-Find-Python3-in-default-env-PATH-for-ACPO.patch
Patch34:	0034-AArch64-Support-HiSilicon-s-HIP09-sched-model.patch

BuildRequires:	binutils-devel
BuildRequires:	cmake
BuildRequires:	gcc
BuildRequires:	gcc-c++
BuildRequires:	libedit-devel
BuildRequires:	libffi-devel
BuildRequires:	multilib-rpm-config
BuildRequires:	ncurses-devel
BuildRequires:	ninja-build
BuildRequires:	python3-devel
BuildRequires:	python3-psutil
BuildRequires:	python3-recommonmark
BuildRequires:	python3-sphinx
BuildRequires:	python3-setuptools
BuildRequires:	zlib-devel
%if %{with toolchain_clang}
BuildRequires:	clang
%endif

Requires:	%{name}-libs%{?_isa} = %{version}-%{release}

Provides:	llvm(major) = %{maj_ver}

%description
LLVM is a compiler infrastructure designed for compile-time, link-time,
runtime, and idle-time optimization of programs from arbitrary programming
languages. The compiler infrastructure includes mirror sets of programming
tools as well as libraries with equivalent functionality.

%package devel
Summary:	Libraries and header files for LLVM
Requires:	%{name}%{?_isa} = %{version}-%{release}
Requires:	%{name}-libs%{?_isa} = %{version}-%{release}
Requires:	libedit-devel
Requires:	%{name}-static%{?_isa} = %{version}-%{release}

%if %{with sys_llvm}
Requires:	%{name}-test%{?_isa} = %{version}-%{release}
Requires:	%{name}-googletest%{?_isa} = %{version}-%{release}
%endif

Requires(post):	%{_sbindir}/alternatives
Requires(postun):	%{_sbindir}/alternatives

Provides:	llvm-devel(major) = %{maj_ver}

%description devel
This package contains library and header files needed to develop new native
programs that use the LLVM infrastructure.

%package doc
Summary:	Documentation for LLVM
BuildArch:	noarch
Requires:	%{name} = %{version}-%{release}
Provides:	%{name}-help = %{version}-%{release}
Obsoletes:	%{name}-help < %{version}-%{release}

%description doc
Documentation for the LLVM compiler infrastructure.

%package libs
Summary:	LLVM shared libraries

%description libs
Shared libraries for the LLVM compiler infrastructure.

%package static
Summary:	LLVM static libraries
Conflicts:	%{name}-devel < 8

Provides:	llvm-static(major) = %{maj_ver}

%description static
Static libraries for the LLVM compiler infrastructure.

%package cmake-utils
Summary: CMake shared utilities

%description cmake-utils
CMake moudules shared between LLVM projects at buid time.
This is for internal use by LLVM packages only.

%package test
Summary:	LLVM regression tests
Requires:	%{name}%{?_isa} = %{version}-%{release}
Requires:	%{name}-libs%{?_isa} = %{version}-%{release}
 
Provides:	llvm-test(major) = %{maj_ver}

%description test
LLVM regression tests.
 
%package googletest
Summary: LLVM's modified googletest sources
 
%description googletest
LLVM's modified googletest sources.

%prep
%setup -T -q -b 1 -n cmake-%{version}.src
cd ..
mv cmake-%{version}.src cmake
%setup -T -q -b 2 -n third-party-%{version}.src
cd ..
mv third-party-%{version}.src third-party
%setup -T -q -b 0 -n llvm-%{version}.src
%autopatch -p2

pathfix.py -i %{__python3} -pn \
	test/BugPoint/compile-custom.ll.py \
	tools/opt-viewer/*.py \
	utils/update_cc_test_checks.py

%build
%if %{with ACPO}
  echo "enable ACPO"
  export CFLAGS="-Wp,-DENABLE_ACPO ${CFLAGS}"
  export CXXFLAGS="-Wp,-DENABLE_ACPO ${CXXFLAGS}"
%endif

%cmake -G Ninja \
	-DBUILD_SHARED_LIBS:BOOL=OFF \
	-DLLVM_PARALLEL_LINK_JOBS=%{max_link_jobs} \
	-DCMAKE_BUILD_TYPE=RelWithDebInfo \
	-DCMAKE_SKIP_RPATH:BOOL=ON \
	-DLLVM_TARGETS_TO_BUILD=%{targets_to_build} \
	-DLLVM_ENABLE_LIBCXX:BOOL=OFF \
	-DLLVM_ENABLE_ZLIB:BOOL=ON \
	-DLLVM_ENABLE_FFI:BOOL=ON \
	-DLLVM_ENABLE_RTTI:BOOL=ON \
	-DLLVM_USE_PERF:BOOL=ON \
	-DLLVM_BINUTILS_INCDIR=%{_includedir} \
	-DLLVM_EXPERIMENTAL_TARGETS_TO_BUILD=%{experimental_targets_to_build} \
	-DLLVM_BUILD_RUNTIME:BOOL=ON \
	-DLLVM_INCLUDE_TOOLS:BOOL=ON \
	-DLLVM_BUILD_TOOLS:BOOL=ON \
	-DLLVM_INCLUDE_TESTS:BOOL=ON \
	-DLLVM_BUILD_TESTS:BOOL=ON \
%if %{with sys_llvm}
	-DLLVM_INSTALL_GTEST:BOOL=ON \
%else
	-DLLVM_INSTALL_GTEST:BOOL=OFF \
%endif
	-DLLVM_LIT_ARGS=-v \
	-DLLVM_INCLUDE_EXAMPLES:BOOL=ON \
	-DLLVM_BUILD_EXAMPLES:BOOL=OFF \
	-DLLVM_INCLUDE_UTILS:BOOL=ON \
	-DLLVM_INSTALL_UTILS:BOOL=ON \
	-DLLVM_INCLUDE_DOCS:BOOL=ON \
	-DLLVM_BUILD_DOCS:BOOL=ON \
	-DLLVM_ENABLE_SPHINX:BOOL=ON \
	-DLLVM_ENABLE_DOXYGEN:BOOL=OFF \
	-DLLVM_BUILD_LLVM_DYLIB:BOOL=ON \
	-DLLVM_LINK_LLVM_DYLIB:BOOL=ON \
	-DLLVM_BUILD_EXTERNAL_COMPILER_RT:BOOL=ON \
	-DLLVM_INSTALL_TOOLCHAIN_ONLY:BOOL=OFF \
	-DLLVM_DEFAULT_TARGET_TRIPLE=%{llvm_triple} \
	-DSPHINX_WARNINGS_AS_ERRORS=OFF \
	-DCMAKE_INSTALL_PREFIX=%{install_prefix} \
	-DLLVM_INSTALL_SPHINX_HTML_DIR=%{install_docdir}/html \
	-DSPHINX_EXECUTABLE=%{_bindir}/sphinx-build-3 \
%if 0%{?__isa_bits} == 64
	-DLLVM_LIBDIR_SUFFIX=64 \
%else
	-DLLVM_LIBDIR_SUFFIX= \
%endif
%if %{with classic_flang}
        -DLLVM_ENABLE_CLASSIC_FLANG=ON \
%endif
%if "%{toolchain}" == "clang"
	-DCMAKE_C_COMPILER=clang \
	-DCMAKE_CXX_COMPILER=clang++ \
%endif
%if %{with bisheng_autotuner}
	-DLLVM_ENABLE_AUTOTUNER=ON \
%endif
	-DLLVM_INCLUDE_BENCHMARKS=OFF
%cmake_build --target LLVM
%cmake_build

%install
%cmake_install

mkdir -p %{buildroot}/%{_bindir}

# Install binaries needed for lit tests
for f in llvm-isel-fuzzer llvm-opt-fuzzer
do
   install -m 0755 %{_builddir}/llvm-%{version}.src/%{__cmake_builddir}/bin/$f %{buildroot}%{install_bindir}
done

%if 0%{?__isa_bits} == 64
install %{_builddir}/llvm-%{version}.src/%{__cmake_builddir}/lib64/libLLVMTestingSupport.a %{buildroot}%{install_libdir}
install %{_builddir}/llvm-%{version}.src/%{__cmake_builddir}/lib64/libLLVMTestingAnnotations.a %{buildroot}%{install_libdir}
%else
install %{_builddir}/llvm-%{version}.src/%{__cmake_builddir}/lib/libLLVMTestingSupport.a %{buildroot}%{install_libdir}
install %{_builddir}/llvm-%{version}.src/%{__cmake_builddir}/lib/libLLVMTestingAnnotations.a %{buildroot}%{install_libdir}
%endif

# Install gtest sources so clang can use them for gtest
install -d %{buildroot}%{install_srcdir}
install -d %{buildroot}%{install_srcdir}/utils/
cp -R ../third-party/unittest %{buildroot}%{install_srcdir}/utils/
 
# Clang needs these for running lit tests.
cp %{_builddir}/llvm-%{version}.src/utils/update_cc_test_checks.py %{buildroot}%{install_srcdir}/utils/
cp -R %{_builddir}/llvm-%{version}.src/utils/UpdateTestChecks %{buildroot}%{install_srcdir}/utils/

# Move header files
mkdir -p %{buildroot}/%{install_includedir}
ln -s ../../../%{install_includedir}/llvm %{buildroot}/%{install_includedir}/llvm
ln -s ../../../%{install_includedir}/llvm-c %{buildroot}/%{install_includedir}/llvm-c

# Fix multi-lib
%multilib_fix_c_header --file %{install_includedir}/llvm/Config/llvm-config.h

# Create ld.so.conf.d entry
mkdir -p %{buildroot}%{_sysconfdir}/ld.so.conf.d
cat >> %{buildroot}%{_sysconfdir}/ld.so.conf.d/%{name}-%{_arch}.conf << EOF
%{install_libdir}
EOF

# Remove opt-viewer, since this is just a compatibility package.
rm -Rf %{build_install_prefix}/share/opt-viewer

mkdir -p %{buildroot}%{install_datadir}/llvm/cmake
cp -Rv ../cmake/* %{buildroot}%{install_datadir}/llvm/cmake

%check
%if %{with check}
LD_LIBRARY_PATH=%{buildroot}/%{install_libdir} %{__ninja} check-all -C %{__cmake_builddir}
%endif

%ldconfig_scriptlets libs

%files
%license LICENSE.TXT
%{install_bindir}/*
%exclude %{install_bindir}/not
%exclude %{install_bindir}/count
%exclude %{install_bindir}/yaml-bench
%exclude %{install_bindir}/lli-child-target
%exclude %{install_bindir}/llvm-isel-fuzzer
%exclude %{install_bindir}/llvm-opt-fuzzer

%files libs
%license LICENSE.TXT
%{install_libdir}/libLLVM-%{maj_ver}.so
%config(noreplace) %{_sysconfdir}/ld.so.conf.d/%{name}-%{_arch}.conf
%{install_libdir}/LLVMgold.so
%{install_libdir}/libLLVM-%{maj_ver}.%{min_ver}*.so
%{install_libdir}/libLTO.so*
%exclude %{install_libdir}/libLTO.so
%{install_libdir}/libRemarks.so*

%files devel
%license LICENSE.TXT
%{install_includedir}/llvm
%{install_includedir}/llvm-c
%{install_libdir}/libLTO.so
%{install_libdir}/libLLVM.so
%{install_libdir}/cmake/llvm

%files doc
%license LICENSE.TXT
%doc %{install_docdir}/html
%{install_prefix}/share/man/man1/*

%files static
%license LICENSE.TXT
%{install_libdir}/*.a
%exclude %{install_libdir}/libLLVMTestingSupport.a
%exclude %{install_libdir}/libLLVMTestingAnnotations.a

%files cmake-utils
%license LICENSE.TXT
%{install_datadir}/llvm/cmake

%files test
%license LICENSE.TXT
%{install_bindir}/not
%{install_bindir}/count
%{install_bindir}/yaml-bench
%{install_bindir}/lli-child-target
%{install_bindir}/llvm-isel-fuzzer
%{install_bindir}/llvm-opt-fuzzer
 
%files googletest
%license LICENSE.TXT
%{install_srcdir}/utils
%{install_libdir}/libLLVMTestingSupport.a
%{install_libdir}/libLLVMTestingAnnotations.a
%{install_libdir}/libllvm_gtest.a
%{install_libdir}/libllvm_gtest_main.a
%{install_includedir}/llvm-gtest
%{install_includedir}/llvm-gmock

%changelog
* Fri Nov 22 2024 xiajingze <xiajingze1@huawei.com> - 17.0.6-29
- [AArch64] Support HiSilicon's HIP09 sched model

* Wed Nov 20 2024 eastb233 <xiezhiheng@huawei.com> - 17.0.6-28
- Find Python3 in default env PATH for ACPO

* Wed Nov 20 2024 eastb233 <xiezhiheng@huawei.com> - 17.0.6-27
- ACPO Infrastructure for ML integration into LLVM compiler

* Wed Nov 20 2024 eastb233 <xiezhiheng@huawei.com> - 17.0.6-26
- [LICM] Solve runtime error caused by the signal function.

* Wed Nov 20 2024 eastb233 <xiezhiheng@huawei.com> - 17.0.6-25
- [SimplifyLibCalls] Merge sqrt into the power of exp (#79146)

* Tue Nov 19 2024 xiajingze <xiajingze1@huawei.com> - 17.0.6-24
- [backport][Clang] Fix crash with -fzero-call-used-regs

* Mon Nov 18 2024 xiajingze <xiajingze1@huawei.com> - 17.0.6-23
- [AArch64] Delete hip09 macro

* Mon Nov 18 2024 liyunfei <liyunfei33@huawei.net> - 17.0.6-22
- Add arch restriction for BiSheng Autotuner

* Mon Nov 18 2024 liyunfei <liyunfei33@huawei.net> - 17.0.6-21
- [Backport] Simple check to ignore Inline asm fwait insertion

* Wed Nov 06 2024 Funda Wang <fundawang@yeah.net> - 17.0.6-20
- adopt to new cmake macro

* Mon Sep 23 2024 zhanglimin <zhanglimin@loongson.cn> - 17.0.6-19
- [LoongArch] Backport some new support

* Thu Sep 12 2024 xiajingze <xiajingze1@huawei.com> - 17.0.6-18
- [AArch64] Support HiSilicon's HIP09 Processor

* Wed Sep 11 2024 hongjinghao <hongjinghao@huawei.com> - 17.0.6-17
- doc add Provides llvm-help

* Tue Sep 10 2024 hongjinghao <hongjinghao@huawei.com> - 17.0.6-16
- doc add Obsoletes llvm-help

* Tue Sep 3 2024 hongjinghao <hongjinghao@huawei.com> - 17.0.6-15
- mv man to doc subpackage

* Mon Jul 22 2024 liyunfei <liyunfei33@huawei.com> - 17.0.6-14
- Prevent environment variables from exceeding NAME_MAX.

* Mon Jul 22 2024 liyunfei <liyunfei33@huawei.com> - 17.0.6-13
- Disable toolchain_clang build for BiSheng Autotuner support temporary.

* Tue Jul 16 2024 liyunfei <liyunfei33@huawei.com> - 17.0.6-12
- Add BiSheng Autotuner support.

* Fri Jul 5 2024 liyunfei <liyunfei33@huawei.com> - 17.0.6-11
- Add toolchain_clang build support

* Mon Apr 29 2024 wangqiang <wangqiang1@kylinos.cn> - 17.0.6-10
- Update llvm-lit config to support macro `build_for_openeuler`

* Sun Apr 21 2024 zhanglimin <zhanglimin@loongson.cn> - 17.0.6-9
- Improve the support for atomic and __clear_cache

* Wed Apr 17 2024 luofeng <luofeng13@huawei.com> - 17.0.6-8
- Add the support for classic flang

* Fri Apr 12 2024 liyunfei <liyunfei33@huawei.com> - 17.0.6-7
- Backport patch to fix CVE-2024-31852

* Thu Apr 11 2024 wangqiang <wangqiang1@kylinos.cn> - 17.0.6-6
- Skip inline asm in inlining target feature check on X86

* Tue Apr 09 2024 liyunfei <liyunfei33@huawei.com> - 17.0.6-5
- Backport patch to fix CVE-2023-46049

* Wed Apr 03 2024 zhanglimin <zhanglimin@loongson.cn> - 17.0.6-4
- Improve the support for compiler-rt and fix some bugs on LoongArch

* Fri Mar 29 2024 zhanglimin <zhanglimin@loongson.cn> - 17.0.6-3
- Add the support for vector on LoongArch

* Sat Mar 16 2024 zhanglimin <zhanglimin@loongson.cn> - 17.0.6-2
- Supoort `relax` feature on LoongArch

* Thu Nov 30 2023 zhoujing <zhoujing106@huawei.com> - 17.0.6-1
- Update to 17.0.6

* Thu Jul 13 2023 cf-zhao <zhaochuanfeng@huawei.com> -12.0.1-7
- Disable check.

* Sat Jul 08 2023 cf-zhao <zhaochuanfeng@huawei.com> -12.0.1-6
- Make this spec file support both system-version and multi-version.

* Tue Feb 14 2023 cf-zhao <zhaochuanfeng@huawei.com> - 12.0.1-5
- Disable check temporarily to avoid a build error that `rmbuild` cannot
- remove a file due to no permission.

* Wed Dec 21 2022 eastb233 <xiezhiheng@huawei.com> - 12.0.1-4
- Type: Compile Option
- ID: NA
- SUG: NA
- DESC: Add -fPIE and -pie options

* Tue Aug 23 2022 guopeilin <guopeilin1@huawei.com> - 12.0.1-3
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: Delete the .so file of old version

* Tue Feb 8 2022 zhangweiguo <zhangweiguo2@huawei.com> - 12.0.1-2
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: Disabe DLLVM_BUILD_TEST

* Mon Dec 13 2021 zoulin <zoulin13@huawei.com> - 12.0.1-1
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: Update version to 12.0.1

* Wed Sep 8 2021 zhangruifang <zhangruifang1@huawei.com> - 10.0.1-4
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: Remove rpath

* Wed Oct 14 2020 Hugel <gengqihu1@huawei.com> - 10.0.1-3
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: Delete the .so file of old version

* Tue Aug 18 2020 Liquor <lirui130@huawei.com> - 10.0.1-2
- Type: bugfix
- ID: NA
- SUG: NA
- DESC:Use -DLLVM_TARGETS_TO_BUILD=all in configure

* Tue Jul 28 2020 Liquor <lirui130@huawei.com> - 10.0.1-1
- Type: update
- ID: NA
- SUG: NA
- DESC:update to 10.0.1

* Wed Jul 22 2020 Hugel <gengqihu1@huawei.com> - 7.0.0-10
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: Ensure that variant part discriminator is read by MetadataLoader
        Fix Assembler/debug-info.ll

* Wed Mar 18 2020 openEuler Buildteam <buildteam@openeuler.org> - 7.0.0-9
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: add four patches

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 7.0.0-8
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: remove unnecessary files

* Tue Dec 31 2019 openEuler Buildteam <buildteam@openeuler.org> -7.0.0-7
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: delete conflict files in llvm

* Fri Nov 1 2019 jiangchuangang <jiangchuangang@huawei.com> -7.0.0-6
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: add libs package

* Mon Oct 28 2019 jiangchuangang <jiangchuangang@huawei.com> -7.0.0-5
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: add test files

* Sun Sep 29 2019 luhuaxin <luhuaxin@huawei.com> - 7.0.0-4
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: add license file

* Fri Sep 20 2019 luhuaxin <luhuaxin@huawei.com> - 7.0.0-3
- Package init
